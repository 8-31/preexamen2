document.addEventListener('DOMContentLoaded', async function () {
    const breedsSelect = document.getElementById('breeds');
    const loadBreedButton = document.getElementById('loadBreedButton');
    const showImageButton = document.getElementById('showImageButton');
    const dogImage = document.getElementById('dogImage');

    async function loadBreeds() {
        try {
            const response = await fetch('https://dog.ceo/api/breeds/list');
            const data = await response.json();
            
            if (data.message) {
                const breeds = data.message;

                breeds.forEach(breed => {
                    const option = document.createElement('option');
                    option.value = breed;
                    option.textContent = breed;
                    breedsSelect.appendChild(option);
                });
            } else {
                console.error('La propiedad "message" no existe en la respuesta:', data);
            }
        } catch (error) {
            console.error('Error al cargar las razas:', error);
        }
    }

    async function loadBreedImage() {
        const selectedBreed = breedsSelect.value;
        try {
            const response = await fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`);
            const data = await response.json();
            const imageUrl = data.message;

            dogImage.src = imageUrl;
        } catch (error) {
            console.error('Error al cargar la imagen:', error);
        }
    }

    if (loadBreedButton) {
        loadBreedButton.addEventListener('click', function () {
            loadBreedImage();
            showImageButton.disabled = false;
        });
    } else {
        console.error('Elemento con ID "loadBreedButton" no encontrado.');
    }

    if (showImageButton) {
        showImageButton.addEventListener('click', loadBreedImage);
    } else {
        console.error('Elemento con ID "showImageButton" no encontrado.');
    }

    loadBreeds();
});
